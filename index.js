[
  {
    	"firstname": "Ron",
		"lastname": "Manzano",
		"email": "canielmanzano@gmail.com",
		"password": "password1",
		"isAdmin": true,
		"mobile": "09151790034"
  }
];

[
  {
    	"productName": "Product 1",
		"description": "First Product",
		"productPrice": "10",
		"stocks": "100",
		"is Active": true,
		"stockKeepingUnit": "SKU01"
  }
];

[
  {
    	"orderID": 1,
		"productID": 1,
		"quantity": 15,
		"orderPrice": 10,
		"subtotal": 150
  }
];

[
  {
    	"userID": 1,
		"transactionDate": "2023-03-03",
		"status": "Completed",
		"total": "150"
  }
];